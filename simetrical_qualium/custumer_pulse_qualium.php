<?php  
/*
* Template Name: Custumer_pulse_qualium
*/
?>
<?php get_header() ?>
<div class="container_mas">
  <a href="#form">
  <img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
  </a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="productosSeccion4">
	<section>
<!-- 		  <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4.png"> -->
		<div class="over">
			<a href="#secdos">
	  			<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>CUSTOMER PULSE:<br>
             <span>
              UN CANAL DIRECTO CON SU CLIENTES
             </span>
           </h2>
             <span class="line"></span>	
  			 <h6> <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
             ?>
  			 </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
			     <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
            <script type="text/javascript">
            function CheckField647796(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
            function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
            function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
            function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
            function CheckFieldD647796(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647796(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {  nDate = ""; retVal = false; } else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
            function validDate647796(chkDD, chkMM, chkYY, frm) {var objDate = null; if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
            function _checkSubmit647796(frm){
            if ( !isemail(frm["fldEmail"].value) ) { 
               alert("Por favor introduzca el Email");
               return false;
            }
             return true; }
            </script>
            <div align="center">
            <form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647796" accept-charset="UTF-8" onsubmit="return _checkSubmit647796(this);" >
            <input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
            <input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
            <input type=hidden name=token value="mFcQnoBFKMStccfMi%2BUOjOZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
            <input type=hidden name=doubleoptin value="" />
            <div class=bmform_outer647796 id=tblFormData647796>
            <div class=bmform_inner647796>
            <div class=bmform_head647796 id=tdHeader647796>
            <div class=bm_headetext647796></div></div>
            <div class=bmform_body647796>
            <div class=bmform_introtxt647796 id=tdIntro647796 >

            </div>
            <div id=tblFieldData647796 style='text-align:left;margin-top: 5%;'>
            <input placeholder=Correo type=text class=bmform_frm647796 name=fldEmail maxlength=100 />
            </div>

            <div class=bmform_button647796><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647796 />
            </div></div>
            <div class=bmform_footer647796><div class=footer_bdy647796><div class=footer_txt647796></div></div></div>
            </div></div>
            </form></div>
            <!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>

			    </div>
  			</div>
		</div>
	</section>
	<section id="secdos">
    <h3 class="hide-for-small-only">
    	¿QUIERE CLIENTES FELICES? ANALICE<BR> 
       DIARIAMENTE SU SATISFACCIÓN
    </h3>
     <h3 class="show-for-small-only">
      ¿QUIERE CLIENTES FELICES? ANALICE
       DIARIAMENTE SU SATISFACCIÓN
    </h3>
    <div class="row" style="position:relative">
      <!-- new slider -->
    <?php $galeria = json_decode(get_post_meta($post->ID,'galeria',true)); ?>
    <div id="containerCa">
     <img id="ipadimagen" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/ipad.png">
      <div id="owl-ipad" class="owl-carousel owl-theme">
        <?php 
          if (count($galeria) > 0): 
            foreach ($galeria as $foto): 
               	echo '<div class="item"><img src="'.$foto->sizes->full.'" ></div>';
            endforeach;	
           endif 
        ?> 
	</div>
	</div>
      <!-- sdcsd -->
  </div>
    <div class="over">
       <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
			  <li>
			  	<h5>Gestión de crisis</h5>
			    <h5 class="sub">No dejes que tu cliente se vaya molesto. Convierte una mala experiencia 
			     en una buena experiencia de atención al cliente.</h5>
			  </li>
			   <li><h5>Información en tiempo real</h5>
			    <h5 class="sub">Olvídate de capturar datos. La información quedará registrada automáticamente
			    	y estará disponible para ti de inmediato.</h5>
			  </li>
			  <li><h5>Integración con Scorecard</h5>
			  	<h5 class="sub">Enlaza tu Customer Pulse con <a class="link" href="<?= get_the_permalink(44) ?>">Scorecard</a> y agrega este indicador a tu tablero de control.
			  	</h5>
			  </li>
		</ul>
    </div>
	</section>
	<section>
        <div class="over">
        <h2>"Una mente llena de dudas no <br>
        	se puede concentrar en una victoria".</h2>
        <h4>-Arthur Golden</h4>
        </div>
	</section>
	<section>
    <h3>CAMBIA LA PERCEPCIÓN DE TUS CLIENTES INSATISFECHOS<BR>
        ANTES DE QUE ABANDONEN TU NEGOCIO
    </h3>
    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
	  <li>
	  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4car.jpg">
	  	<h4 id="custumer">Concesionarias <br>
        automotrices</h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4ham.jpg">
		<h4 id="custumer">Cadenas de <br>
        restaurantes</h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccionbol.jpg">

	  	<h4 id="custumer">
	  		Sitios de <br>
    E-Commerce</h4></li>
	</ul>
	</section>
    <section id="form">
    <h3>QUIERO TENER CLIENTES<br>
    	SATISFECHOS</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>
	
<?php get_footer() ?>