$(document).ready(function() {
  $("#owl-ipad").owlCarousel({
      navigation : true, // Show next and prev buttons
      singleItem:true,
      loop:true,
      items:1,
      autoplay:true,
      autoplayTimeout:9000,
      slideSpeed : 9000,
      paginationSpeed :9000,
      loop:true,
      dots:false,
      touchDrag:false,
      mouseDrag:false,
      responsiveClass:true,

  });
  //
$('.owl-carousel').owlCarousel({
    dots:true,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:9000,
    slideSpeed : 9000,
    paginationSpeed :9000,
    responsiveClass:true,
    stopOnHover:true,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        600:{
            items:1,
            nav:false,
        },
        1000:{
            items:1,
            nav:false,
        }
    }
})
});