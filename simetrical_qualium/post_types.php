<?php 
	add_action( 'init', 'create_post_type' );
	function create_post_type()
	{
		register_post_type('HomeTestimonios',
        	array(
            	'labels' => array(
                'name' => __('HomeTestimonios'),
                'singular_name' => __('HomeTestimonio')
            	),
            	'public' => true,
            	'has_archive' => true,
            	//'taxonomies' => array('category'),
            	'supports' => array('title','editor','thumbnail')
        	)
    	);

        register_post_type('ScoreTestimonios',
            array(
                'labels' => array(
                'name' => __('ScoreTestimonios'),
                'singular_name' => __('ScoreTestimonio')
                ),
                'public' => true,
                'has_archive' => true,
                //'taxonomies' => array('category'),
                'supports' => array('title','editor','thumbnail')
            )
        );
	}
?>