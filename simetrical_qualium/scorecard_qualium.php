<?php  
/*
* Template Name: Scorecard_qualium
*/
?>
<?php get_header() ?>
<div class="container_mas">
	<a href="#form">
	<img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
	</a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="productosSeccion2">
	<section>
		<div class="over">
			<a href="#secdos">
  			 <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>SCORECARD: <span> EL SECRETO DEL<br>
              EMPRESARIO EXITOSO </span></h2>
             <span class="line"></span>	
  			 <h6>
  			    <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
  			     ?>
  			  </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
					<!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
					<script type="text/javascript">
					function CheckField647790(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
					function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
					function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
					function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
					function CheckFieldD647790(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647790(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
					function validDate647790(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
					function _checkSubmit647790(frm){
					if ( !isemail(frm["fldEmail"].value) ) { 
					   alert("Por favor introduzca el Email");
					   return false;
					}
					 return true; }
					</script>
					<div align="center">
					<form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647790" accept-charset="UTF-8" onsubmit="return _checkSubmit647790(this);" >
					<input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
					<input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
					<input type=hidden name=token value="mFcQnoBFKMQPcQoXIi%2F0BOZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
					<input type=hidden name=doubleoptin value="" />
					<div class=bmform_outer647790 id=tblFormData647790>
					<div class=bmform_inner647790>
					<div class=bmform_head647790 id=tdHeader647790>
					<div class=bm_headetext647790></div></div>
					<div class=bmform_body647790>
					<div class=bmform_introtxt647790 id=tdIntro647790 >

					</div>
					<div id=tblFieldData647790 style='text-align:left;margin-top: 5%;'>
					<input placeholder=Correo type=text class=bmform_frm647790 name=fldEmail maxlength=100 />
					</div>

					<div class=bmform_button647790><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647790 />
					</div></div>
					<div class=bmform_footer647790><div class=footer_bdy647790><div class=footer_txt647790></div></div></div>
					</div></div>
					</form></div>
					<!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>

			</div>
		</div>
	</section>
	<section id="secdos">
	    <h3 class="hide-for-small-only">ÓPTICAS DE NEGOCIOS ACTUALIZADAS <br>
	        DONDE SEA QUE ESTÉS
	    </h3>
	    <h3 class="show-for-small-only">ÓPTICAS DE NEGOCIOS ACTUALIZADAS 
	        DONDE SEA QUE ESTÉS
	    </h3>
	<div class="row" style="position:relative">
	     <!-- new slider -->
    <?php $galeria = json_decode(get_post_meta($post->ID,'galeria',true)); ?>
    <div id="containerCa">
     <img id="ipadimagen" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/ipad.png">
      <div id="owl-ipad" class="owl-carousel owl-theme">
        <?php 
          if (count($galeria) > 0): 
            foreach ($galeria as $foto): 
               	echo '<div class="item"><img src="'.$foto->sizes->full.'" ></div>';
            endforeach;	
           endif 
        ?> 
	</div>
	</div>
      <!-- sdcsd -->
    </div>
	   <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
            <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/doc.png" class="image img1">
		  	<h5>Información actualizada</h5>
		  	   <h6>Nuestro sistema automatizado recopilará tu información 
		  	   	durante la noche, mientras descansas. Cuando 
		  	    despiertes tu información estará actualizada en todos 
		  	     tus dispositivos.</h6>
		  </li>
		  <li>
            <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/grafica.png" class="image img2">
		  	<h5>Escoge tus indicadores</h5>
              <h6>Demasiada información puede resultar abrumadora, pero 
		  	   	  Scorecard te permite seleccionar los indicadores que más te
		  	      interesan para tenerlos en tu tablero de control principal.
		  	      Podrás acceder desde PC, Mac, iPhone o iPad.
		  </li>
		  <li>
		  <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/like.png" class="image img3">
		  	<h5>Toma mejores decisiones</h5>
              <h6>La información se puede analizar de manera general, por 
              	  unidad de negocio, departamento, indicador y detalle de las 
                  transacciones, lo que permite estar al tanto en todo 
                  momento, en cualquier parte del mundo.
                  </h6>
		  </li>
		</ul>
	</section>
	<section>
		<?php  $args = array(
							'post_type'   => 'ScoreTestimonios',
							'order'=>'ASC'
		 				);
					$query = new WP_Query($args); ?> 		
		<div class="owl-carousel">
				<?php while ($query->have_posts()): $query->the_post(); ?>     
				<div class="item">
					     <h3><?= get_the_content(get_the_ID()); ?></h2>
					     <h4><?= get_the_title(get_the_ID()); ?></h3>
				  </div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>	
	   </div>
	</section>
	<section>
         <h4>TODOS NECESITAN SER MÁS PRODUCTIVOS</h4>
         <h6>Trabajamos con todo tipo de industrias. No importa si eres un <br>
         	hospital, gobierno o universidad, Scorecard te ayudará <br>
         	a tomar mejores decisiones.</h6>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
		  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen1.png">
		  	<h3>Concesionarias <br>
		  	 automotrices</h3>
		  </li>
		  <li>
		  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen2.png">
		  	<h3>Cadenas de<br>
		  		restaurantes</h3>
		  </li>
		   <li>
		   	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen3.png">
		  	<h3>Sitios de <br>
		  	 E-Commerce</h3>
		  </li>
		</ul>
	</section>
	<section id="form">
    <h3>CONVIÉRTETE EN UN EMPRESARIO <br>
    	REALMENTE EXITOSO</h3>
    	<?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>
<?php get_footer() ?>