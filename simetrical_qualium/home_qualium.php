<?php 
/*
* Template Name: Home_qualium
*/
 ?>
<?php get_header() ?>
<script type="text/javascript">
jQuery( document ).ready(function() {
  function gup( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp ( regexS );
        var tmpURL = window.location.href;
        var results = regex.exec( tmpURL );
        if( results == null )
            return"";
        else
            return results[1];
    } 
if (gup('contacto') == 'form') {
var total = parseInt(jQuery('.productos').height() - jQuery('section:nth-of-type(5)').height());
jQuery('body').animate({scrollTop:total}, '1000');
};
});
</script>
<div class="container_mas">
	<a href="#form">
	<img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
	</a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="productos">
	<section id="seccion1">
		<video id="video" loop="loop" autoplay="autoplay">
			 <source type="video/mp4" src="<?php echo site_url() ?>/wp-content/uploads/2015/01/Scorecard319202.mp4">
			 <source type="video/webm" src="<?php echo site_url() ?>/wp-content/uploads/2015/01/Scorecard3_1920.webmhd.webm"> 
		</video>
		<!-- <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion_uno.jpg"> -->
		<div class="over">

		    <a href="#secdos">
			<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			<a>
			<div id="container_text">
            <h2>PARA TOMAR LAS MEJORES DECISIONES,<BR>
             UN EMPRESARIO REQUIERE SABER DIARIAMENTE <BR>
             LO QUE OCURRE EN SU EMPRESA<br>
             <span class="line"></span>	
             <div>
  			 <h6>Creemos firmemente que ayudando a empresarios visionarios y vanguardistas como tú a tener ópticas actualizadas de negocio, ponemos nuestro grano de arena en un proyecto que culminará en incremento de utilidades y un crecimiento de la empresa.</h6>
  			 </div>
  			 </div>
		</div>
	</section>
	<section id="secdos">
             <h3>HAY MUCHO QUE PODEMOS HACER POR TI</h3>
             <h6>Contamos con diversos productos que te ayudarán a monitorear los
             	indicadores de negocio más importantes.</h6>
             <h6>Nuestro compromiso: la optimización y maximización de tu empresa pues nos
             	apasiona generar métricas, ayudándote a crecer.</h6>
              <div class="container_carru">
              <a id="arrowrigth">
              <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png?>" class="arrowrigth">
			  </a>
              <a id="arrowleft">
			   <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png?>" class="arrowleft">
			  </a> 
			<div id="items">
			<?php $args = array(
				'order'   => 'ASC',
				'orderby' => 'ID',
				'parent' => get_the_ID()
			); 
			$pages = get_pages($args); 
			sort($pages);
			?>
			<?php $conta = 0; ?>
			<?php foreach ($pages as $key => $value): ?>
			<img draggable="false" id="item" src="<?= wp_get_attachment_url(get_post_thumbnail_id($value->ID)) ?>"> 
			<?php  $conta++; ?>
			<?php endforeach ?>
			</div>
			</div>
			 <?php $conta = 0; ?>
			 <?php foreach ($pages as $key => $value): ?>
			 <div id="link<?= $conta?>" class="product">
              <h3 id="custumer"><?php echo $value->post_title ?></h3>
              <h6><?=  $value->post_content ?></h6>
               <a href="<?= get_the_permalink($value->ID) ?>" class="Btn-Green button" >Más información</a>
     		</div>
             <?php  $conta++; ?>
			<?php endforeach ?>
			<?php wp_reset_query(); ?>

	</section>
	<!-- <section class="text-center">
			<?php 
					$args = array(
							'post_type'   => 'hometestimonios',
							'order'=>'ASC'
		 				);
					$query = new WP_Query($args); 
				?>
				<div class="over2"> </div>
			   <div class="owl-carousel">
				<?php while ($query->have_posts()): $query->the_post(); ?>
     
			<div class="item" style="background:url(<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) ?>)">
			    <div class="over">
				     <h3><?= get_the_content(get_the_ID()); ?></h3>
				     <h4><?= get_the_title(get_the_ID()); ?></h4>
		        </div>
			  </div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>	
				</div>
	</section> -->	
	<section class="text-center">
			<?php $args = array(
							'post_type'   => 'hometestimonios',
							'order'=>'ASC'
		 				);
					$query = new WP_Query($args); 
				?>
				<div class="over2"> </div>
				<?php while ($query->have_posts()): $query->the_post(); ?>
     
			<div class="item" style="background:url(<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) ?>)">
			<?php if (get_the_title(get_the_ID()) == '-Warren Buffett'): ?>
					<div class="over">
				     <h2><?= get_the_content(get_the_ID()); ?></h2>
				     <h3><?= get_the_title(get_the_ID()); ?></h3>
		        </div>
			<?php endif ?>
			  </div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>	
	</section>
	<section>
            <h3>MARCAS QUE HAN CONFIADO EN NOSOTROS</h3>
            <ul class="small-block-grid-1 medium-block-grid-5 large-block-grid-5">
			  <li><img id="logo_clientes1" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/honda.png"></li>
			  <li><img id="logo_clientes1" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/nissan.png"></li>
			  <li><img id="logo_clientes1" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/toyota.png"></li>
			  <li><img id="logo_clientes1" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/susuki.png"></li>
			  <li><img id="logo_clientes1" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/volkswagen.png"></li>
			</ul>
			 <ul class="small-block-grid-1 medium-block-grid-5 large-block-grid-5">
			  <li><img id="logo_clientes2" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-01.svg"></li>
			  <li><img id="logo_clientes2" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-02.svg"></li>
			  <li><img id="logo_clientes2" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-03.svg"></li>
			  <li><img id="logo_clientes2" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-04.svg"></li>
			  <li><img id="logo_clientes2" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-05.svg"></li>
			</ul>
			 <ul class="small-block-grid-1 medium-block-grid-5 large-block-grid-5">
			  <li><img id="logo_clientes3" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-06.svg"></li>
			  <li><img id="logo_clientes3" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-07.svg"></li>
			  <li><img id="logo_clientes3" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-08.svg"></li>
			  <li><img id="logo_clientes3" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-09.svg"></li>
			  <li><img id="logo_clientes3" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/Logo-10.svg"></li>
			</ul>
	</section>
	<section id="form">
           <h3>¿QUIERES TENER ÓPTICAS DIARIAS DE TU NEGOCIO?</h3>
           <h6>Déjanos tus datos y un asesor se comunicará contigo en menos 
            de 24 horas</h6>
            <fieldset>
            <div  class="small-10 small-offset-1 medium-6 medium-offset-3  large-offset-3  large-6 columns">
			<?php 
			echo do_shortcode('[contact-form-7 id="1357" title="form_climaorganizacional"]');
			 ?>
			</div>
           </fieldset>
	</section>
</div>
<?php get_footer() ?>