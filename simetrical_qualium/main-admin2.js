jQuery(document).ready(function() {
    jQuery('#basics').removeClass('hidden');
    jQuery('.color-field').wpColorPicker();

    var formfield;

    /* /user clicks button on custom field, runs below code that opens new window */
    var custom_uploader;
    var images = [];
    var currentImages;
    var position;
    jQuery('.onetarek-upload-button').click(function(e) {
        var contador = parseInt( jQuery(this).data('contador') );
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Seleccionar imagen',
            button: {
                text: 'Seleccione la(s) Imágene(s)'
            },
            multiple: true
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').toJSON();
            var value = jQuery('#image_location').val();
            if(value != ""){
                currentImages = JSON.parse(value);
                images = currentImages;
            }

            jQuery.each( attachment, function( i, val ) {
                var sizes = {
                    image : val.name,
                    sizes : {
                        thumbnail : val.sizes.thumbnail.url,
                        medium : val.sizes.medium.url,
                        full : val.sizes.full.url
                    }
                };
                //create images sizes array
                images.push(sizes);
                jQuery('.items').append('<div class="img_'+i+' content-img" data-position="'+i+'"><img src="'+val.sizes.thumbnail.url+'"><a class="remove_img">Borrar</a></div>');

            });            

            jQuery('#image_location').val(JSON.stringify(images));
            jQuery(".remove_img").click(function(){

                var position=parseInt(jQuery(this).data('position'));
                delete images[position];
                jQuery(this).parent().remove();
                jQuery('#image_location').attr('value',JSON.stringify(images));                
                //jQuery('#image_location').val(JSON.stringify(images));
            });

            //console.log(images)

        });

        //Open the uploader dialog
        custom_uploader.open();

    });

    jQuery(".remove_img").click(function(){

        if( confirm("¿Esta seguro que desea eliminar esta imagen?") ){
            var arr = JSON.parse(jQuery('#image_location').val());
            var position=parseInt(jQuery(this).data('position'));
            arr.splice(position,1);
            jQuery(this).parent().remove();
            console.log(arr);
            //jQuery('#image_location').val(JSON.stringify(arr));
            jQuery('#image_location').attr('value', JSON.stringify(arr));
        }        
        
    });


    /*
     Please keep these line to use this code snipet in your project
     Developed by oneTarek http://onetarek.com
     */
    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };

    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data

    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };
});
//
jQuery("#wysija-tabs .nav-tab" ).click(function(e) {
    jQuery('#wysija-settings .wysija-panel').addClass('hidden');
    jQuery('#wysija-settings '+e.target.hash+'').removeClass('hidden');
});
//