<?php  
/*
* Template Name: customer watch
*/
?>
<?php get_header() ?>
<div class="container_mas">
  <a href="#form">
  <img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
  </a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="customer_watch">
	<section>
		<div class="over">
			<a href="#secdos">
  			 <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2> CUSTOMER WATCH: 
            <span> RECUPERA A TUS CLIENTES <B>E INCREMENTA</B> LA FIDELIDAD DE LOS MISMOS</span></h2>
             <span class="line"></span>	
  			 <h6>
  			    <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
  			     ?>
  			  </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
			     <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
			 <script type="text/javascript">
			function CheckField647813(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
			function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
			function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
			function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
			function CheckFieldD647813(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647813(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
			function validDate647813(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
			function _checkSubmit647813(frm){
			if ( !isemail(frm["fldEmail"].value) ) { 
			   alert("Por favor introduzca el Email");
			   return false;
			}
			 return true; }
			</script>
			<div align="center">
			<form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647813" accept-charset="UTF-8" onsubmit="return _checkSubmit647813(this);" >
			<input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
			<input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
			<input type=hidden name=token value="mFcQnoBFKMS7354ghHJQGuZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
			<input type=hidden name=doubleoptin value="" />
			<div class=bmform_outer647813 id=tblFormData647813>
			<div class=bmform_inner647813>
			<div class=bmform_head647813 id=tdHeader647813>
			<div class=bm_headetext647813></div></div>
			<div class=bmform_body647813>
			<div class=bmform_introtxt647813 id=tdIntro647813 >

			</div>
			<div id=tblFieldData647813 style='text-align:left;margin-top:5%'>
			<input placeholder=Correo type=text class=bmform_frm647813 name=fldEmail maxlength=100 />
			</div>

			<div class=bmform_button647813><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647813 />
			</div></div>
			<div class=bmform_footer647813><div class=footer_bdy647813><div class=footer_txt647813></div></div></div>
			</div></div>
			</form></div>
			<!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
			</div>
		</div>
	</section>
<section id="secdos">
           <h3 style="padding-bottom: 45px;" class="hide-for-small-only">CONSERVAR TUS CLIENTES ES LA MANERA MÁS <BR>
           BARATA DE SEGUIR GANANDO DINERO
     	   </h3>
     	   <div class="imagen_fondo"></div>
           <div class="over">
            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-2">
               <h3 class="show-for-small-only">CONSERVAR TUS CLIENTES ES LA MANERA MÁS <BR>
           BARATA DE SEGUIR GANANDO DINERO</h3>
			  <li>
			  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> -->
			    <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
			    Incrementa la retención y las utilidades con una carga de datos sencilla.</h5>
			     </li>
			  <li>
			  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> -->
			    <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
			   Nosotros te proporcionamos la herramienta para que lo logres por tu cuenta.</h5>
			     </li>
			  <li>
			  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> --><!-- 
			    <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
			    Si lo hacemos nosotros, te proporcionamos un reporte ejecutivo con los resultados del incremento en tu retención.</h5>
			     </li> -->
			</ul>
           </div>
	</section>
	<section>
               <div class="over">
               <h2>“No hay nada más caro que captar nuevos clientes, <br>
               mejor conserva los que ya tienes”.</h2>
               </div>
	</section>
	<section>
	<!-- 	    <h3>TODOS NECESITAN SABER LO QUE <BR>
        SUS CLIENTES QUIEREN.
    </h3> -->
	<div class="row">
	<div class="large-6 medium-6 small-12 columns">
	    <ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-1">
	  <li>
	  	<img src="<?php echo get_template_directory_uri()?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro8.jpg">
	  <!-- 	<h4 id="titulo">Trabajamos Con:</h4>
	  	<h6>Concesionarias de Autos</h4> -->
	  </li>
	</ul>
	</div>
	<div class="large-6 medium-6 small-12 columns texto2">
		 <h4 id="titulo">Trabajamos con:</h4>
	  	<h4 id="titulo2">Concesionarias de autos</h4> 
	</div>
	</div>
	</section>
	<section id="form">
          <h3>QUIERO CONVERTIR A MIS CLIENTES <br>
    	EN CONSUMIDORES FIELES</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
	</section>
</div>
<?php get_footer() ?>