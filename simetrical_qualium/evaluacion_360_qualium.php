<?php  
/*
* Template Name: Evaluacion_360_qualium
*/
?>
<?php get_header() ?>
<div class="container_mas">
	<a href="#form">
	<img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
	</a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="productosSeccion3">
	<section>
 <!--  <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion3.jpg"> -->
		<div class="over">
			<a href="#secdos">
	  		<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>EVALUACIÓN DE 360 GRADOS:<br>
              <span>EL EMPLEADO CORRECTO EN <br>
              EL PUESTO CORRECTO</span>
           </h2>
             <span class="line"></span>	
  			 <h6><?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
  			     ?>
  			 </h6>
  			 <div class="small-12 medium-7 medium-offset-3 large-6 large-offset-3 columns ">
			      <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
					<script type="text/javascript">
					function CheckField647801(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
					function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
					function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
					function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
					function CheckFieldD647801(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647801(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
					function validDate647801(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
					function _checkSubmit647801(frm){
					if ( !isemail(frm["fldEmail"].value) ) { 
					   alert("Por favor introduzca el Email");
					   return false;
					}
					 return true; }
					</script>
					<div align="center">
					<form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647801" accept-charset="UTF-8" onsubmit="return _checkSubmit647801(this);" >
					<input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
					<input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
					<input type=hidden name=token value="mFcQnoBFKMSSeA61xylPFOZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
					<input type=hidden name=doubleoptin value="" />
					<div class=bmform_outer647801 id=tblFormData647801>
					<div class=bmform_inner647801>
					<div class=bmform_head647801 id=tdHeader647801>
					<div class=bm_headetext647801></div></div>
					<div class=bmform_body647801>
					<div class=bmform_introtxt647801 id=tdIntro647801 >

					</div>
					<div id=tblFieldData647801 style='text-align:left;margin-top: 5%;'>
					<input placeholder=Correo type=text class=bmform_frm647801 name=fldEmail maxlength=100 />
					</div>

					<div class=bmform_button647801><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647801 />
					</div></div>
					<div class=bmform_footer647801><div class=footer_bdy647801><div class=footer_txt647801></div></div></div>
					</div></div>
					</form></div>
					<!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
			    </div>
  			</div>
		</div>
	</section>
	<section id="secdos">
           <h3 class="hide-for-small-only">TOMA MEJORES DECISIONES</h3>
           <div class="over">
            <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
               <h3 class="show-for-small-only">TOMA MEJORES DECISIONES</h3>
			  <li>
			  	<h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Información invaluable</h5>
			    <h5 class="sub">Conocer a fondo las habilidades y debilidades de tu 
			    	equipo es una ventaja que todo directivo necesita tener. 
			    	Mientras más información, mejores decisiones.</h5></li>
			  <li><h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Fácil aplicación</h5>
			    <h5 class="sub">Sus empleados recibirán un correo electrónico con una liga 
			    que deberán seguir para realizar la evaluación, que no dura	
			    más de 10 minutos.</h5></li>
			  <li><h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Evaluados por todos</h5>
			  	<h5 class="sub">Para la obtención de resultados certeros, los gerentes y 
			  		personal clave son evaluados por todo su entorno: jefes, 
			  		pares y subordinados.</h5></li>
			</ul>
           </div>
	</section>
	<section>
	<h4>AUTOEVALUACIÓN EN <BR>
		11 COMPETENCIAS CLAVE</h4>
	<div class="row">
		<div class="small-12 medium-6 large-6 columns">
		    <ul class="small-block-grid-1">
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Aprendizaje</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Compromiso</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Comunicación</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Disponibilidad</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Liderazgo</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Manejo del conflicto</li>
			</ul>
		</div>
		<div class="small-12 medium-6 large-6 columns">
		    <ul class="small-block-grid-1">
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Orientación</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Orientación al servicio</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Proactividad</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Solución de problemas</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Trabajo en equipo</li>
			</ul>
		</div>
	</div>
	</section>
	<section>
               <div class="over">
               <h2>"El camino al éxito es medir".</h2>
               </div>
	</section>
	<section>
	   <div class="row">
          <div class="small-5 medium-7 large-7 columns image">
    <!-- new slider -->
    <?php $galeria = json_decode(get_post_meta($post->ID,'galeria',true)); ?>
    <div id="containerCa">
     <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/ipad.png">
      <div id="owl-ipad" class="owl-carousel owl-theme">
        <?php 
          if (count($galeria) > 0): 
            foreach ($galeria as $foto): 
               	echo '<div class="item"><img src="'.$foto->sizes->full.'" ></div>';
            endforeach;	
           endif 
        ?> 
	</div>
	</div>
      <!-- sdcsd -->
          </div>
          <div class="small-7 medium-5 large-5 columns text">
          	       <h3>Analiza los resultados</h3>
          	       <h6>Los resultados son analizados en una <br>
          	       	herramienta sencilla que le permite identificar, <br>
          	       	a nivel de cada gerente en particular o del <br>
          	        conjunto gerencial, las fortalezas y debilidades, <br>
          	        estableciendo planes de acción dirigidos y personalizados.
          	     </h6>
          </div>
	   </div>
	</section>
	<section id="form">
    <h3>QUIERO SABER SI MI GERENTE ESTÁ <br>
    	EN EL PUESTO ADECUADO</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>

<?php get_footer() ?>