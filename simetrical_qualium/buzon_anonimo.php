<?php
/*
* Template Name: buzon anonimo
*/
get_header();
?>
<div class="container_mas">
  <a href="#form">
  <img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
  </a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="buzon_anonimo">
    <section>
        <div class="over">
            <a href="#secdos">
             <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
            </a>
            <div id="container_text">
            <h2>BUZÓN ANÓNIMO: 
            <span> UN CANAL DE COMUNICACIÓN DIRECTO CON TU EMPRESA</span></h2>
             <span class="line"></span> 
             <h6>
                <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
                 ?>
              </h6>
                <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
                <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>

                <script type="text/javascript">
                function CheckField647814(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
                function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
                function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
                function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
                function CheckFieldD647814(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647814(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {  nDate = ""; retVal = false; } else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
                function validDate647814(chkDD, chkMM, chkYY, frm) {var objDate = null; if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
                function _checkSubmit647814(frm){
                if ( !isemail(frm["fldEmail"].value) ) { 
                   alert("Por favor introduzca el Email");
                   return false;
                }
                 return true; }
                </script>
                <div align="center">
                <form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647814" accept-charset="UTF-8" onsubmit="return _checkSubmit647814(this);" >
                <input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
                <input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
                <input type=hidden name=token value="mFcQnoBFKMTExXEfPFZp6%2BZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
                <input type=hidden name=doubleoptin value="" />
                <div class=bmform_outer647814 id=tblFormData647814>
                <div class=bmform_inner647814>
                <div class=bmform_head647814 id=tdHeader647814>
                <div class=bm_headetext647814></div></div>
                <div class=bmform_body647814>
                <div class=bmform_introtxt647814 id=tdIntro647814 >

                </div>
                <div id=tblFieldData647814 style='text-align:left;margin-top:5%'>

                <input placeholder=Correo type=text class=bmform_frm647814 name=fldEmail maxlength=100 />
                </div>

                <div class=bmform_button647814><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647814 />
                </div></div>
                <div class=bmform_footer647814><div class=footer_bdy647814><div class=footer_txt647814></div></div></div>
                </div></div>
                </form></div>
                <!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>

            </div>
        </div>
    </section>
<section id="secdos">
           <h3 style="padding-bottom:45px;" class="hide-for-small-only">LAS OPINIONES DE TODOS SON IMPORTANTES <BR>
      PARA LOGRAR NUESTRAS METAS
           </h3>
           <div class="imagen_fondo"></div>
           <div class="over">
            <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
               <h3 class="show-for-small-only">LAS OPINIONES DE TODOS SON IMPORTANTES <BR>
           PARA LOGRAR NUESTRAS METAS</h3>
              <li>
                <!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> -->
                <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
                Es un medio para que el personal de una empresa u organización exprese diferentes situaciones laborales y así poder detectar todo lo que ocurre entorno a ella.</h5>
                 </li>
              <li>
                <!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> -->
                <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
               Únicamente el dueño de la empresa puede visualizar e interpretar la información obtenida a través de esta herramienta.</h5>
                 </li>
              <li>
                <!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem nipsum</h5> -->
                <h5 class="sub"><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
                La identidad de los empleados nunca será revelada y podrán expresar sus dudas, ideas, quejas, denuncias y sugerencias libremente.</h5>
                 </li>
            </ul>
           </div>
    </section>
    <section>
               <div class="over">
               <h2>“La habilidad en el arte de la comunicación es crucial para el éxito de un líder".</h2>
               </div>
    </section>
    <section>
            <h3>TODOS NECESITAN CONOCER SU EMPRESA</h3>
            <h4 class="subtitulo">Trabajamos con todo tipo de empresas públicas, hospitales, universidades, etc.</h4>   
    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
      <li>
        <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro1.jpg">
        <h4 id="titulo">Empresas públicas</h4>
      </li>
      <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro3.jpg">
        <h4 id="titulo">Hospitales </h4>
      </li>
      <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro4.jpg">
        <h4 id="titulo">Universidades</h4></li>
    </ul>
    </section>
      <section id="form">
          <h3>QUIERO TENER UN CANAL DE COMUNICACIÓN CON <br>
        TODOS LOS COLABORADORES DE MI EMPRESA</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>
<?php get_footer() ?>