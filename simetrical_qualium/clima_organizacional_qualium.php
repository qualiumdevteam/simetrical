<?php  
/*
* Template Name: clima organizacional qualium
*/
?>
<?php get_header() ?>
<div class="container_mas">
	<a href="#form">
	<img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
	</a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="clima_organizacional">
	<section >
	<!-- <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/clima_fondo.jpg"> -->
			<div class="over">
			<a href="#secdos">
  			 <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>CLIMA ORGANIZACIONAL: 
            <span> El ÉXITO DE LAS GRANDES EMPRESAS</span></h2>
             <span class="line"></span>	
  			 <h6>
  			    <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
  			     ?>
  			  </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
			    <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
					<script type="text/javascript">
					function CheckField647804(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
					function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
					function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
					function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
					function CheckFieldD647804(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647804(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
					function validDate647804(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
					function _checkSubmit647804(frm){
					if ( !isemail(frm["fldEmail"].value) ) { 
					   alert("Por favor introduzca el Email");
					   return false;
					}
					 return true; }
					</script>
					<div align="center">
					<form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647804" accept-charset="UTF-8" onsubmit="return _checkSubmit647804(this);" >
					<input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
					<input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
					<input type=hidden name=token value="mFcQnoBFKMSAfhJOLzdzsuZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
					<input type=hidden name=doubleoptin value="" />
					<div class=bmform_outer647804 id=tblFormData647804>
					<div class=bmform_inner647804>
					<div class=bmform_head647804 id=tdHeader647804>
					<div class=bm_headetext647804></div></div>
					<div class=bmform_body647804>
					<div class=bmform_introtxt647804 id=tdIntro647804 >

					</div>
					<div id=tblFieldData647804 style='text-align:left;margin-top: 5%;'>
					<input placeholder=Correo type=text class=bmform_frm647804 name=fldEmail maxlength=100 />
					</div>

					<div class=bmform_button647804><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647804 />
					</div></div>
					<div class=bmform_footer647804><div class=footer_bdy647804><div class=footer_txt647804></div></div></div>
					</div></div>
					</form></div>
					<!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
			</div>
		</div>
	</section>
		<section id="secdos">
	    <h3 class="hide-for-small-only">COMPARA TUS RESULTADOS CON UN ESTÁNDAR<br>
		GENERAL DE LAS EMPRESAS EN TU MISMA RAMA
	    </h3>
	    <h3 class="show-for-small-only">COMPARA TUS RESULTADOS CON UN ESTÁNDAR 
		GENERAL DE LAS EMPRESAS EN TU MISMA RAMA
	    </h3>
	     <!-- new slider -->
    <?php $galeria = json_decode(get_post_meta($post->ID,'galeria',true)); ?>
    <div id="containerCa">
     <img id="ipadimagen" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/clima_org.png">
     <!--  <div id="owl-ipad" class="owl-carousel owl-theme">
        <?php 
          if (count($galeria) > 0): 
            foreach ($galeria as $foto): 
               	echo '<div class="item"><img src="'.$foto->sizes->full.'" ></div>';
            endforeach;	
           endif 
        ?> 
	</div> -->
	</div>
      <!-- sdcsd -->
	   <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
            <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/doc.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5>
		  	 --> <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/doc.png" class="image">  -->
		  	 <h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
		  	 Encuesta realizada por internet que obtiene de forma anónima la opinión de los empleados con respecto a la relación laboral.</h6>
		  </li>
		  <li>
            <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/grafica.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5> -->
              	<h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
              	Los resultados se presentan con comparativos con otras unidades de negocio y contra el mercado.</h6>
		  </li>
		  <li>
		  <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/like.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5>
             --> <h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
             Identificar claramente la cultura de la organización, al igual que las áreas y segmentos de los empleados con problemas.</h6>
		  </li>
		</ul>
	</section>
	<section>
               <div class="over">
               <h2>“Conociendo el nivel de satisfacción de nuestros empleados podemos predecir el nivel de satisfacción de nuestros clientes”.</h2>
               </div>
	</section>
	<section>
		    <h3>TODOS NECESITAN CONOCER SU EMPRESA</h3>
		    <h4 class="subtitulo">Trabajamos con todo tipo de empresas públicas, hospitales, universidades, etc.</h4>	
    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
	  <li>
	  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro1.jpg">
	  	<h4 id="titulo">Empresas públicas</h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro3.jpg">
		<h4 id="titulo">Hospitales </h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro4.jpg">
	  	<h4 id="titulo">Universidades</h4></li>
	</ul>
	</section>
		<section id="form">
          <h3>QUIERO CONOCER MEJOR <br>
    	A MIS EMPLEADOS</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
	</section>
</div>
<?php get_footer() ?>