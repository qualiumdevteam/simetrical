<?php  
/*
* Template Name: operation board
*/
?>
<?php get_header() ?>
<div class="container_mas">
  <a href="#form">
  <img src="http://www.simetrical.com/wp-content/themes/Divi/simetrical_qualium/assets/INFO.svg" class="arrow_mas">
  </a>
</div>
<div class="mas_info"><p >Más información</p></div>
<div class="operation_board">
	<section >
<!-- 	<img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imgs_cambios_2016/fondo_operation.jpg"> -->
		<div class="over">
			<a href="#secdos">
  			 <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>OPERATION BOARD: 
            <span> EVALÚA Y MONITOREA LA EFICIENCIA DE TUS GERENTES</span></h2>
             <span class="line"></span>	
  			 <h6>
  			    <?php 
                 while (have_posts()): the_post();
                 the_content();
                 endwhile;
  			     ?>
  			  </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-6 large-offset-3 columns ">
			    <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
					<script type="text/javascript">
					function CheckField647808(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
					function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
					function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
					function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
					function CheckFieldD647808(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate647808(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
					function validDate647808(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
					function _checkSubmit647808(frm){
					if ( !isemail(frm["fldEmail"].value) ) { 
					   alert("Por favor introduzca el Email");
					   return false;
					}
					 return true; }
					</script>
					<div align="center">
					<form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB647808" accept-charset="UTF-8" onsubmit="return _checkSubmit647808(this);" >
					<input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
					<input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
					<input type=hidden name=token value="mFcQnoBFKMQhOO9HHwtDA%2BZXUawFafLCm3LEjvJmSyISQX%2Fl5TlnOA%3D%3D" />
					<input type=hidden name=doubleoptin value="" />
					<div class=bmform_outer647808 id=tblFormData647808>
					<div class=bmform_inner647808>
					<div class=bmform_head647808 id=tdHeader647808>
					<div class=bm_headetext647808></div></div>
					<div class=bmform_body647808>
					<div class=bmform_introtxt647808 id=tdIntro647808 >

					</div>
					<div id=tblFieldData647808 style='text-align:left;margin-top: 5%;'>

					<input placeholder=Correo type=text class=bmform_frm647808 name=fldEmail maxlength=100 />
					</div>

					<div class=bmform_button647808><input type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit647808 />
					</div></div>
					<div class=bmform_footer647808><div class=footer_bdy647808><div class=footer_txt647808></div></div></div>
					</div></div>
					</form></div>
					<!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
			</div>
		</div>
	</section>
	<section id="secdos">
	    <h3 class="hide-for-small-only">ESCOGE LOS OBJETIVOS DEPARTAMENTALES QUE QUIERES ALCANZAR, PONDÉRALOS Y SUPERVÍSALOS 
	    </h3>
	    <h3 class="show-for-small-only">ESCOGE LOS OBJETIVOS DEPARTAMENTALES QUE QUIERES ALCANZAR,PONDÉRALOS Y SUPERVÍSALOS
	    </h3>
	     <!-- new slider -->
	<div class="row" style="position: relative;">
	   <?php $galeria = json_decode(get_post_meta($post->ID,'galeria',true)); ?>
    <div id="containerCa">
     <img id="ipadimagen" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/ipad.png">
      <div id="owl-ipad" class="owl-carousel owl-theme">
        <?php 
          if (count($galeria) > 0): 
            foreach ($galeria as $foto): 
               	echo '<div class="item"><img src="'.$foto->sizes->full.'" ></div>';
            endforeach;	
           endif 
        ?> 
	</div>
	</div>
      <!-- sdcsd -->
	</div>
	   <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
            <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/doc.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5> -->
		  	   <h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
		  	   Obtendrás porcentajes de eficiencia ligados al sueldo variable de cada gerente.</h6>
		  </li>
		  <li>
            <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/grafica.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5> -->
              <h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
		  	   Compara objetivos generados contra objetivos elegibles y establece indicadores específicos.</h6>
		  </li>
		  <li>
		  <!-- <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/like.png" class="image"> -->
		  	<!-- <h5><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Lorem ipsun</h5> -->
             <h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">
		  	  Le permite al cliente ver los objetivos y resultados de las operaciones.</h6>
		  </li>
		</ul>
	</section>
	<section>
               <div class="over">
               <h2>“El éxito no se logra sólo con cualidades especiales, es sobre todo un trabajo de constancia, método y organización”.</h2>
               </div>
	</section>
	<section>
	<!-- 	    <h3>TODOS NECESITAN SABER LO QUE <BR>
        SUS CLIENTES QUIEREN.
    </h3> -->
	<div class="row">
	<div class="large-6 medium-6 small-12 columns">
	    <ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-1">
	  <li>
	  	<img src="<?php echo get_template_directory_uri()?>/simetrical_qualium/assets/imgs_cambios_2016/cuadro5.jpg">
	  <!-- 	<h4 id="titulo">Trabajamos Con:</h4>
	  	<h6>Concesionarias de Autos</h4> -->
	  </li>
	</ul>
	</div>
	<div class="large-6 medium-6 small-12 columns texto2">
		 <h4 id="titulo">Trabajamos con:</h4>
	  	<h4 id="titulo2">Concesionarias de autos</h4> 
	</div>
	</div>
	</section>
	<section id="form">
          <h3>QUIERO CONTAR CON MAS INFORMACIÓN <BR>
          DE MI NEGOCIO</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos tres datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
	</section>
</div>
<?php get_footer() ?>